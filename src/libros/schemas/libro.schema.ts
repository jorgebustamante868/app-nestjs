import * as mongoosee from 'mongoose'

export const LibroSchema = new mongoosee.Schema({
    titulo: String,
    autor:String,
    descripcion:String
});